const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()

app.use(bodyParser.json())
app.use(cors())

// const employees = require('./data/employees.json');
const Employees = require('./models').Employee

var corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
}

/**
 * Find All
 * @param {Object} req
 * @param {Object} res
 */
app.get('/api/employees', cors(corsOptions), async (req, res) => {
  console.log('GET /api/employees');
  res.setHeader('Content-Type', 'application/json');

  try {
    const employees = await Employees.findAll({});

    res.status(200).send(JSON.stringify(employees))

  } catch(e) {
    res.status(500).send(JSON.stringify(e));
  }
});

/**
 * FindOne - Find an employee by ID
 * @param {Object} req
 * @param {Object} res
 */
app.get('/api/employees/:id', cors(corsOptions), async (req, res) => {
  console.log('GET /api/employees/', req.params.id);
  res.setHeader('Content-Type', 'application/json');

  try {
    const employee = await Employees.findOne({
      where: {
        id: req.params.id
      }
    });

    res.status(200).send(JSON.stringify(employee))

  } catch(e) {
    res.status(500).send(JSON.stringify(e));
  }
});

/**
 * Create - Create an employee
 * @param {Object} req
 * @param {Object} res
*/
app.post('/api/employees', cors(corsOptions), async (req, res, next) => {
  console.log('POST /api/employees');
  res.setHeader('Content-Type', 'application/json');

  try {

    const employee = await Employees.create({
      name: req.body.name,
      code: req.body.code,
      profession: req.body.profession,
      color: req.body.color,
      city: req.body.city,
      branch: req.body.branch,
      assigned: req.body.assigned
    });

    res.status(201).send(JSON.stringify(employee))

  } catch(e) {
    res.status(500).send(JSON.stringify(e));
  }
});

/**
 * /update - Update an employee
 * @param {Object} req
 * @param {Object} res
 */
app.put('/api/employees/:id', cors(corsOptions), async (req, res, next) => {
  res.setHeader('Content-Type', 'application/json');
  res.status(200);

  console.log(req.body)

  try {
    const employees = await Employees.update(req.body, {
      where: { id: req.params.id }
    });

    res.status(200).send(JSON.stringify(employees))

  } catch(e) {
    res.status(500).send(JSON.stringify(e));
  }
});

/**
 * Delete - Delete an employee
 * @param {Object} req
 * @param {Object} res
 */
app.delete('/api/employees/:id', cors(corsOptions), async (req, res, next) => {
  res.setHeader('Content-Type', 'application/json');

  try {
    const employees = await Employees.destroy({ where: { id: req.params.id } });

    res.status(200).send(JSON.stringify(employees))

  } catch(e) {
    res.status(500).send(JSON.stringify(e));
  }

});

app.listen(8080, () => console.log('Job Dispatch API running on port 8080!'))

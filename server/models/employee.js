'use strict';
module.exports = (sequelize, DataTypes) => {
  const Employee = sequelize.define('Employee', {
    name: DataTypes.STRING,
    code: DataTypes.STRING,
    profession: DataTypes.STRING,
    color: DataTypes.STRING,
    city: DataTypes.STRING,
    branch: DataTypes.STRING,
    assigned: {
      type: DataTypes.BOOLEAN,
      defaultValue: 0,
      allowNull: true
    }
  }, {});
  Employee.associate = function(models) {
    // associations can be defined here
  };
  return Employee;
};

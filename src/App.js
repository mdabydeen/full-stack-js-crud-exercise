import React, { useState, useEffect } from 'react';
import Table from './Table'
import EmployeeModal from './EmployeeModal';

function App() {
  const [employees, setEmployees] = useState([])
  const [modalShow, setModalShow] = useState(false);
  const [title, setTitle] = useState(null);
  const [data, setData] = useState(null);

  function fetchData() {
    fetch('http://localhost:8080/api/employees')
      .then(response => response.json())
      .then(employees => {
        setEmployees(employees)
      })
  }

  useEffect(() => fetchData(), [])

  function handleEdit(e, row) {
    setData(row._original)
    setTitle('Edit')
    setModalShow(true);
  }

  function deleteItem(e, row) {
    fetch(`http://localhost:8080/api/employees/${row._original.id}`, {
      method: 'DELETE'
    })
    .then(response => response.json())
    .then(employees => {
      //setEmployees(employees)
      fetchData();
    })
  }

  const columns = React.useMemo(
    () => [
      {
        columns: [
          {
            Header: 'Name',
            accessor: 'name',
          },
          {
            Header: 'Code',
            accessor: 'code',
          },
          {
            Header: 'Profession',
            accessor: 'profession',
          },
          {
            Header: 'Color',
            accessor: 'color',
            getProps: (state, rowInfo, column) => {
              return {
                style: {
                  background: rowInfo && rowInfo.row.color,
                  color: 'white'
                },
              }
            },
            Cell: ({ row }) => ('')
          },
          {
            Header: 'City',
            accessor: 'city',
          },
          {
            Header: 'Branch',
            accessor: 'branch',
          },
          {
            Header: 'Assigned',
            accessor: 'assigned',
            Cell: ({ row }) => ( row.assigned ? 'YES': 'NO')
          },
          {
            Header: '',
            id: 'edit-button',
            Cell: ({ row }) => (<button className="btn btn-primary btn-large" onClick={(e) => handleEdit(e, row)}>Edit</button>)
          },
          {
            Header: '',
            id: 'delete-button',
            Cell: ({ row }) => (<button className="btn btn-danger btn-large" onClick={(e) => deleteItem(e, row)}>Delete</button>)
          }
        ],
      },
    ],
    []
  )

  return (
    <div className="App">
      <h1> Plexxis Employees</h1>
      <button className="btn btn-primary"  onClick={() => {
        setModalShow(true),
        setTitle('Add')
      }}
      > Add New Employee</button>
      <Table columns={columns} data={employees} />
      <EmployeeModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        title={title}
        data={data}
      />
    </div>
  )
}

export default App;

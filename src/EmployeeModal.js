import React, { useState } from 'react'
import { Button, Modal } from 'react-bootstrap'
import ModalForm from './ModalForm'

function addEmployee(data) {
  fetch('http://localhost:8080/api/employees', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      name: data.name,
      code: data.code,
      profession: data.profession,
      color: data.color,
      city: data.city,
      branch: data.branch,
      assigned: data.assigned
    })
  })
  .then(response => response.json())
  .then(employee => {
    console.log(employee)
    return employee;
  })
}


function editEmployee(data, id) {
  console.log('editEmployee', data);

  fetch(`http://localhost:8080/api/employees/${id}`, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
  .then(response => response.json())
  .then(employee => {
    return employee;
  })
}


function EmployeeModal(props) {
    const [values, setValues] = useState({})

    function handleModal() {
      props.onHide(false);
    }

    function addOrEditEmployee() {
      switch(props.title) {
        case 'Add':
          addEmployee(values);
          break;
        case 'Edit':
          editEmployee(values, props.data.id);
          break;
        default:
          break;
      }

      return;
    }

    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            {props.title} Employee
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ModalForm
          data={props.data}
          setValues={setValues}
        />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={ () => {
            handleModal()
            addOrEditEmployee()
          }}>{props.title}</Button>
        </Modal.Footer>
      </Modal>
    );
}

export default EmployeeModal

import React from 'react';
import ReactTable from "react-table";
import "react-table/react-table.css";

function Table({ columns, data }) {

  function getTrProps(state, rowInfo, instance) {
    if (rowInfo) {
      return {
        style: {
          textAlign: 'center'
        }
      }
    }
    return {};
  }

  return (
    <ReactTable
    columns={columns}
    data={data}
    defaultPageSize={10}
    className="-striped -highlight"
    getTrProps={getTrProps}
    />
  )
}

export default Table;

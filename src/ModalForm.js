import React, { useState, useEffect } from 'react';
import { Form, Col } from 'react-bootstrap'

function ModalForm(props) {
  const [values, setValues] = useState({})

  useEffect(() => {
    if (props.data) {
      setValues(props.data)
    } else {
      setValues({})
    }
  }, [])

  return (
  <Form>
    <Form.Row>
      <Form.Group as={Col} controlId="formGridName">
        <Form.Label>Name</Form.Label>
        <Form.Control
          type="hidden"
          defaultValue={values.id}
          onChange={e => props.setValues({ id: values.id })}
        />
        <Form.Control
          type="text"
          placeholder="Enter Full Name"
          defaultValue={values.name}
          onChange={e => props.setValues({ name: e.target.value })}
        />
      </Form.Group>

      <Form.Group as={Col} controlId="formGridProfession">
        <Form.Label>Profession</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Profession"
          defaultValue={values.profession}
          onChange={e => props.setValues({ profession: e.target.value })}
        />
      </Form.Group>
    </Form.Row>

    <Form.Row>
      <Form.Group as={Col} controlId="formGridColor">
        <Form.Label>Color</Form.Label>
        <Form.Control
          type="text"
          defaultValue={values.color}
          onChange={e => setValues({ color: e.target.value })}
        />
      </Form.Group>

      <Form.Group as={Col} controlId="formGridState">
        <Form.Label>Assigned</Form.Label>
        <Form.Control as="select"
          defaultValue={values.assigned}
          onChange={e => setValues({ assigned: e.target.value })}
          >
          <option>Choose...</option>
          <option value={1}>Yes</option>
          <option value={0}>No</option>
        </Form.Control>
      </Form.Group>
    </Form.Row>
    <Form.Row>
      <Form.Group as={Col} controlId="formGridCity">
        <Form.Label>City</Form.Label>
        <Form.Control
          type="text"
          defaultValue={values.city}
          onChange={e => setValues({ city: e.target.value })}
        />
      </Form.Group>

      <Form.Group as={Col} controlId="formGridCode">
        <Form.Label>Code</Form.Label>
        <Form.Control
          type="text"
          defaultValue={values.code}
          onChange={e => setValues({ code: e.target.value })}
        />
      </Form.Group>

      <Form.Group as={Col} controlId="formGridBranch">
        <Form.Label>Branch</Form.Label>
        <Form.Control
          type="text"
          defaultValue={values.branch}
          onChange={e => setValues({ branch: e.target.value })}
        />
      </Form.Group>
    </Form.Row>

  </Form>
  )
}

export default ModalForm;

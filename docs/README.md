# Solving the Exercise
by Michael Dabydeen (<mdabydeen@gmail.com>)

## Approach

I've taken a full stack approach and focused on the following:

### Stack:
- React (FE)
- Express (Node.js) Backend
- MariaDB

### Build Environment
- Used Docker to build a demo stack with the technologies defined above.

### Backend
- Added CRUD Endpoints for the following:
  - creating : `POST /api/employees/`
  - edit employees : `PUT /api/employees/:id`
  - deleteing employees : `DELETE /api/employees/:id`

Used [Sequelize]() ORM for DB modeling and management

#### Future Considerations:
- Add a security / auth layer, such that only authenticated users can add or modify employees

- Add a caching layer (`redis`) to handle large amounts of traffic - even though that would be an overkill for this example.

### Front-End
Added Components for [React Table]() View

Added buttons / function to edit/add/delete employees

Added Modal / Form Component for add/edit

#### Future Considerations:
  - Can add state management  like Redux, Flux, Mobx etc for better data handling over the app, even though it can be an overkill for this example.
  - Add buttons/pages to authenticate users to login
  before making changes to the app

### Database
Use `MariaDB` as the tool of choice because of personal preference.
  - Can be easily swapped out for any DB
  - Use Sequelize CLI for migrations of DB tables.

#### Future Considerations:
  - Add replication / caching if application size grows.

### Extras

- Used `editorconfig` & `prettier` for standardized formatting
- Used `gitflow` process for merging and development

#### Future Considerations:
  - Add CI/CD build tools

## __How to build (with Docker)__

Requirements:

1. [Docker](https://www.docker.com/products/docker-desktop)

With docker installed, clone this repo

```
$ git clone git@bitbucket.org:mdabydeen/full-stack-js-crud-exercise.git exercise
```

then navigate into the cloned directory

```
$ cd exercise/
```

then run the `docker` command

```
$ docker-compose up -d
```

This will run the following:

1. Pull all relevant images from docker hub
2. Build a container for the front-end
3. Build a container for the backend (`server/`)
4. Setup a database container running [mariadb](https://mariadb.com/)
5. Setup a container with `adminer` - a phpMyAdmin style
viewer for your database.

Once the containers are up and running.

1. Navigate to `http://localhost:8080` for the front-end
2. Navigate to `https://localhost:3000` for the backend
3. Navigate to `https://localhost:9000` for the db view

## __How to build (without Docker)__

Requirements:

1. [Nodejs](https://nodejs.org/en/download/)
2. [Mariadb](https://mariadb.com/downloads/) / [MySQL](https://www.mysql.com/products/)
3. [Sequelize-CLI](https://github.com/sequelize/cli#installation)

Steps to setup:

#### Config

##### Setup the DB
Using `mariadb` create a database, let's call it `plexxis` for this case.

##### Modify and Export your configs

1. Copy the `vars.env.example` file into `vars.env`
2. Modify your `var.env` file with the correct info for your database
3. Run the follow command:
```
$ source vars.env
```

##### Running Migrations

For migrations, you will need to:

```
$ cd server
```

then run

```
$ npx sequelize-cli db:migrate
```

### Start your engines

Run the stack by using the following command

```
$ npm start
```

Once the containers are up and running.

1. Navigate to `http://localhost:8080` for the front-end
2. Navigate to `https://localhost:3000` for the backend

